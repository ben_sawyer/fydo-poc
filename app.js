var path = require('path'),
    models = require('./models'),
    api  = require('./routes/api'),
    auth = require('./routes/auth'),
    morgan = require('morgan'),
    express = require('express');

var app = express();

app.use(morgan(process.env.NODE_ENV === 'development' ? 'dev' : 'common'));
app.use('/auth', auth);
app.use('/api', api);
app.use(express.static(path.join(__dirname, 'static')));

models.sequelize.sync({force: process.env.NODE_ENV === 'development'})
    .then(function () {
        app.listen(process.env.PORT);
        console.log('Application initialization successful. Server listening on port ' + process.env.PORT + '.');
    });
