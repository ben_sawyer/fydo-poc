import Signal from './signal';
let auth,
    token = null;
auth = {
    update: new Signal()
};
Object.defineProperties(auth, {
    token: {
        get: function(){
            return token === null ? token = JSON.parse(window.localStorage.getItem('auth_token')) : token;
        },
        set: function(value){
            if(token === value) return;
            token = value;
            if(token === null){
                window.localStorage.removeItem('auth_token');
            }else{
                window.localStorage.setItem('auth_token', JSON.stringify(value));
            }
            this.update(token);
        }
    }
});
export default auth;
