import state from '../state';
let nav;
nav = {
    ready: function(results, app){
        //console.log('nav:ready', results);
    },
    enter: function(){
        main.addEventListener('click', mainClick);
    },
    leave: function(current){
        main.removeEventListener('click', mainClick);
    },
    load: function(app){
        main.addEventListener('click', mainClick);
    }
};
function mainClick(){
    state.current = state.previous;
}
export default nav;
