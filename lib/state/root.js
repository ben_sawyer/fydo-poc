import Ajax from '../ajax';
let root;
root = {
    ready: function(results, app){
        //console.log('root:ready', results);
    },
    enter: function(previous){
        //console.log('root:enter, previous state was ' + previous);
    },
    leave: function(current){
        //console.log('root:leave, current state is ' + current);
    },
    load: function(app){
        //console.log('root:load');
    }
};
export default root;
