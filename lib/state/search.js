import Map from '../map';
import geo from '../geo';
import Ajax from '../ajax';

let search,
    map = new Map(window.searchMap),
    options = {
        uri: '/search',
        params: {
            term: '',
            ll: undefined
        }
    },
    ajax = new Ajax(options),
    debounce;

// //make this a component
// var searchResultsList = windsock.compile(windsock.parse(window.searchResults).children[0]);
// windsock.transclude(searchResultsList, window.searchResults); //hack for fragment return

map.options.mapTypeControl = false;
map.options.streetViewControl = false;

// map.listen('bounds_changed', ()=>{
//     ajax.options.params.ll = map.lat + ',' + map.lng;
//     ajax.options.params.term = ''; //searchInput.value
//     if(debounce){
//         window.clearTimeout(debounce);
//     }
//     debounce = window.setTimeout(()=>{
//         ajax.GET()
//             .then((data)=>{
//                 let ulFragment = windsock.parse(JSON.parse(data)),
//                     compiled = windsock.compile(ulFragment),
//                     marker,
//                     li;
//
//                 while(searchResultsList.children.length){
//                     searchResultsList.children[0].remove();
//                 }
//
//                 map.clear();
//
//                 while(compiled.children[0].children.length){
//                     li = compiled.children[0].children[0];
//                     //li.on('click', appendSearchResultClone);
//                     searchResultsList.append(li);
//                     marker = map.marker(li.attributes.lat, li.attributes.long);
//                     //marker.addListener('click', markerClick.bind(null, windsock.compile(li.clone(true)), marker));
//                 }
//             });
//     }, 500);
// });

search = {
    enter:{
        location: function(){
            if(geo.location.position && geo.location.address){
                return geo.location;
            }
            return geo.getCurrent();
        },
        map: function(){
            return map.init();
        }
    },
    leave: function(current){
        console.log('search:current state is ' + current);
    },
    load: {
        location: function(){
            if(geo.location.position && geo.location.address){
                return geo.location;
            }
            return geo.getCurrent();
        },
        map: function(){
            return map.init();
        }
    },
    ready: function(){
        console.log('search state ready');
        map.zoom = 13;
        map.center = {
            lat: geo.location.position.coords.latitude,
            lng: geo.location.position.coords.longitude
        };
    }
};
export default search;
