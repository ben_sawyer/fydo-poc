export default class Viewmodel{
    constructor(){
        this.root = this.root || document;
        this.source = undefined;
        this.template = undefined;
        this.vdom = undefined;
        this.dependencies = this.constructor.dependencies.map((dep)=>{
            return new dep();
        });
    }
    querySelector(){
        var str = [];
        switch (Object.prototype.toString.call(this.constructor.selector)) {
            case '[object Function]':
                this.constructor.selector(str);
                break;
            case '[object Object]':
                for(var key in this.constructor.selector){
                    str.push('[' + key + (this.constructor.selector[key] ? '="' + this.constructor.selector[key] + '"]' : ']'));
                }
                break;
            default:
                str.push(this.constructor.selector);
        }
        return str.join(',');
    }
    query(){
        this.source = Array.prototype.slice.call(this.root.querySelectorAll(this.querySelector()));
        this.source.forEach((node)=>{
            this.dependencies.forEach((dep)=>{
                dep.root = node;
                dep.query();
            });
        });
        return this;
    }
    parse(){
        this.template = this.source.map((source)=>{
            return windsock.parse(source);
        });
        this.template.forEach((template)=>{
            this.dependencies.forEach((dep)=>{
                dep.parse(template.filter(dep.selector));
            });
        });
        return this;
    }
    compile(){
        this.vdom = this.template.map((template)=>{
            return windsock.compile(template);
        });
        this.vdom.forEach((vdom)=>{
            this.dependencies.forEach((dep)=>{
                dep.compile(vdom.filter(dep.selector));
            });
        });
        return this;
    }
    transclude(){
        this.vdom.forEach((vdom)=>{
            return windsock.transclude(vdom);
        });
        return this;
    }
    render(){
        this.query()
            .parse()
            .compile()
            .transclude();
        return this.vdom;
    }
    destroy(){
        this.vdom.destroy();
    }
}
Viewmodel.selector = '';
Viewmodel.dependencies = [];
