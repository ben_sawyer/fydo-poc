let toast,
    queue = [];
toast = {
    vm: undefined,
    simple: function(message){
        if(queue.push(message) <= 1) display();
    },
    error: function(){
        //have a different queue for errors
    }
};
function display(){
    if(queue.length) toast.vm.display(queue.shift()).then(display);
}
export default toast;
