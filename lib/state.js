import Signal from './signal';
let state,
    current;
state = {
    create: function(){
        return new State();
    },
    previous: '',
    change: new Signal(),
    load: new Signal(),
    ready: new Signal(),
    resolve: function(promise){
        if(Object.prototype.toString.call(promise) === '[object Array]'){
            state.map = state.map.concat(promise);
        }else{
            state.map.unshift(promise);
        }
    }
};
class State {
    constructor(){
        this.enter = new Signal();
        this.leave = new Signal();
        this.load = new Signal();
        this.ready = new Signal();
    }
    destroy(){
        this.enter.remove();
        this.leave.remove();
        this.load.remove();
        this.ready.remove();
    }
}
Object.defineProperties(state, {
    current:{
        get: function(){
            return current;
        },
        set: function(value){
            location.hash = value;
        },
        enumerable: true
    }
});
window.addEventListener('hashchange', ()=>{
    state.map = [];
    state.previous = state.current;
    current = location.hash.substr(1);
    state.change.dispatch(current, state.previous);
    //state map is updated individually by each listener of of state.load
    Promise.all(state.map)
        .then((results)=>{
            state.ready.dispatch(current, results);
        });
});
window.addEventListener('load', ()=>{
    state.map = [];
    current = location.hash.substr(1) || '';
    state.load.dispatch(current);
    //state map is updated individually by each listener of of state.load
    //by invoking state.resolve(promise|promises)
    Promise.all(state.map)
        .then((results)=>{
            state.ready.dispatch(current, results);
        });
});
export default state;
