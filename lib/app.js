import state from './state';
let app,
    states = {
        root: undefined
    },
    viewmodels = [];
app = {
    init: function(){
        app.state.init();
        app.vm.init();
    },
    destroy: function(){
        for(var name in states){
            states[name].destroy();
        }
        state.change.remove();
    },
    state:{
        init: function(){
            state.ready.add((current, results)=>{
                var currentState = app.state.resolve(current);
                currentState && currentState.ready.dispatch(results, app);
            });
            state.load.add((current)=>{
                var initialState = app.state.resolve(current);
                if(initialState){
                    state.resolve(initialState.load.dispatch(app));
                }
            });
            state.change.add((current, previous)=>{
                var currentState = app.state.resolve(current),
                    previousState = app.state.resolve(previous);
                previousState && previousState.leave.dispatch(current, app);
                if(currentState){
                    state.resolve(currentState.enter.dispatch(previous, app));
                }
            });
            //state load get session
            //and any request that requires a session cookie needs to handle failures
        },
        register: function(name, config){
            if(!name){
                throw new Error('failed to register state, missing name');
            }
            return registerState(name, config);
        },
        resolve: function(name){
            return name.length ? states[name] : states.root;
        },
        go: function(request){
            if(states[request]){
                state.current = request;
            }
        }
    },
    vm:{
        init: function(){
            viewmodels.forEach((Viewmodel)=>{
                var vm = new Viewmodel();
                vm.render();
            });
        },
        add: function(Viewmodel){
            viewmodels.push(Viewmodel);
        }
    }
};
function registerState(stateName, config){
    states[stateName] = state.create();
    Object.keys(config)
        .forEach((signal)=>{
            if(!states[stateName][signal]){
                throw new Error('failed to register state, signal ' + signal + ' does not exist');
            }
            if(Object.prototype.toString.call(config[signal]) === '[object Function]'){
                states[stateName][signal].add(config[signal]);
            }else{
                for(var key in config[signal]){
                    states[stateName][signal].add(config[signal][key]);
                }
            }
        });
    return states[name];
}
export default app;
