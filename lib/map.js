var map = Symbol();
export default class Map{
    constructor(element, center = new google.maps.LatLng(-34, 151), zoom = 1){
        this.listeners = [];
        this.markers = [];
        this.element = element;
        this.options = {
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            center: center,
            zoom: zoom
        };
    }
    init(){
        return new Promise((resolve, reject)=>{
            let evt;
            if(this[map]){
                resolve(this);
            }else{
                //google.maps.event.addDomListener(window, 'load', ()=>{});
                //perform async op here
                Map.extend(this);
                while(this.listeners.length){
                    evt = this.listeners.shift();
                    evt.resolve(this[map].addListener(evt.name, evt.callback));
                }
                resolve(this);
            }
        });
    }
    static extend(obj){
        obj[map] = new google.maps.Map(obj.element, obj.options);
    }
    static marker(instance, lat, long){
        let _marker = new google.maps.Marker({
            position: new google.maps.LatLng(lat, long),
            map: instance[map],
            icon:{
                path: 'M31.75,0C48.318,0,61,12.488,61,29.057V30c0,21.834-19.322,49-29.75,55H31C20.572,79,0,51.834,0,30v-0.943  C0,12.488,13.932,0,30.5,0C30.667,0,31.583,0,31.75,0z',
                fillColor: '#f93526',
                fillOpacity: 1,
                scale: 0.33,
                strokeColor: '#df1f11',
                strokeWeight: 1
            }
        });
        instance.markers.push(_marker);
        return _marker;
    }
    marker(lat, long){
        return Map.marker(this, lat, long);
    }
    clear(){
        this.markers.forEach((marker)=>{
            marker.setMap(null);
        });
    }
    listen(name, callback){
        return new Promise((resolve, reject)=>{
            if(!this[map]){
                this.listeners.push({
                    name: name,
                    callback: callback,
                    resolve: resolve
                });
            }else{
                resolve(this[map].addListener(name, callback));
            }
        });
    }
    bounds(){
        let b = this[map].getBounds(),
            sw= b.getSouthWest(),
            ne = b.getNorthEast();
        return {
            'sw_latitude': ne.lat().toString(),
            'sw_longitude': sw.lng().toString(),
            'ne_latitude': ne.lat().toString(),
            'ne_longitude': ne.lng().toString()
        };
    }
    get lat(){
        return this[map].getCenter().lat();
    }
    get lng(){
        return this[map].getCenter().lng();
    }
    get center(){
        var c = this[map].getCenter();
        return {
            lat: c.lat(),
            lng: c.lng()
        };
    }
    set center(value){
        this[map].setCenter(new google.maps.LatLng(value.lat, value.lng));
    }
    get zoom(){
        var c = this[map].getCenter();
        return this[map].zoom;
    }
    set zoom(value){
        this[map].setZoom(value);
    }
}
