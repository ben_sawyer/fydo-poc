import auth from './auth';
export default class Ajax{
    constructor(options){
        this.options = options;
    }
    get url(){
        return this.options.uri + Ajax.params(this.options.params);
    }
    GET(){
        return Ajax.request(this.url);
    }
    POST(data){
        return Ajax.request(this.url, 'POST', data);
    }
    PUT(data){
        return Ajax.request(this.url, 'PUT', data);
    }
    DELETE(){
        return Ajax.request(this.url, 'DELETE');
    }
    static request(url, method='GET', data={}){
        return new Promise((resolve, reject) => {
            let client = new XMLHttpRequest();
            client.open(method, url, true);
            if(method === 'POST' || method === 'PUT'){
                client.setRequestHeader('Content-Type', 'application/json');
            }
            client.send(JSON.stringify(data));
            client.onload = function(){
                if(this.status >= 200 && this.status < 300){
                    resolve(JSON.parse(this.response));
                }else{
                    reject(JSON.parse(this.response), this);
                    if(this.status === 401){
                        auth.token = null;
                    }
                }
            };
            client.onerror = function(){
                reject(this.statusText);
            };
        });
    }
    static params(params={}){
        return Object.keys(params)
            .map((key, i)=>{
                let param = i === 0 ? '?' : '&';
                param += key;
                if(params[key]) param += '=' + params[key];
                return param;
            })
            .join('');
    }
}
