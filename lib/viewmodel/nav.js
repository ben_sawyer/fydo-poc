import Viewmodel from '../viewmodel';
import auth from '../auth';

export default class Nav extends Viewmodel{
    constructor(){
        super();
    }
}
Nav.selector = 'nav';
Nav.dependencies = [];
