import Viewmodel from '../viewmodel';
import Ajax from '../ajax';
import auth from '../auth';
import state from '../state';
import Field from './field';
import toast from '../toast';

export default class Signin extends Viewmodel{
    constructor(){
        super();
        this.ajax = new Ajax({
            uri: '/api/auth'
        });
    }
    compile(){
        super.compile();
        this.vdom.forEach((vdom)=>{
            let form = vdom.find('form');
            form.on('submit', (evt)=>{
                let body = {};
                evt.preventDefault();
                form.filter('input').forEach((input)=>{
                    body[input.attributes.name] = input.node.value;
                });
                this.ajax.POST(body)
                    .then((response)=>{
                        //write auth to localstorage
                        //redirect to root
                        auth.token = response.token;
                        state.current = '';
                    },(e)=>{
                        toast.simple(e.message);
                    });
            });
        });
        return this;
    }
}
Signin.selector = {
    signin: null
};
Signin.dependencies = [Field];
