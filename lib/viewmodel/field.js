import Viewmodel from '../viewmodel';

export default class Field extends Viewmodel{
    constructor(){
        super();
        this.selector = 'field';
    }
    //this could be a potential problem:
    //if the field is rendered as a root vm
    //the vdom will be an array of fragments
    //if the vdom is rendered as a dependent
    //the vdom will be an array of elements
    parse(template){
        if(!template){
            super.parse();
        }else{
            this.template = template;
        }
        this.template.forEach((temp)=>{
            if(!temp.attributes['has-value']){
                temp.attributes['has-value'] = 'false';
            }
        });
        return this;
    }
    compile(vdom){
        if(!vdom){
            super.compile();
        }else{
            this.vdom = vdom;
        }
        this.vdom.forEach((v)=>{
            let input = v.find('input');
            input.on('blur', (evt)=>{
                v.attributes['has-value'] = !!input.node.value.length;
            });
        });
        return this;
    }
}
Field.selector = 'field';
Field.dependencies = [];
