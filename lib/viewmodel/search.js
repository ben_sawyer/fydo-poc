import Viewmodel from '../viewmodel';
import service from '../service/search';

export default class Search extends Viewmodel{
    constructor(selectors, type){
        super(selectors);
        this.delay = 500;
        this.debounce = undefined;
        this.type = type;
        this.value = '';
    }
    compile(){
        super.compile();
        this.vdom.forEach((vdom)=>{
            vdom.children[0].on('keyup', ()=>{
                this.value = vdom.children[0].node.value;//update this value across other vdoms
                if(this.debounce){
                    window.clearTimeout(this.debounce);
                }
                this.debounce = window.setTimeout(()=>{
                    service.search.dispatch(this);
                },this.delay);
            });
        });
        return this;
    }
}
