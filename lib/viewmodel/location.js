import Viewmodel from '../viewmodel';
import Ajax from '../ajax';
import service from '../service/search';

export default class Location extends Viewmodel{
    constructor(input){
        let debounce;
        super(['[location]','.location-list']);
        this.ajax = new Ajax({
            uri: '/location',
            params: {
                input: ''
            }
        });
        service.search.add((input)=>{
            if(input.type === 'location'){
                this.ajax.options.params.input = input.value;
                this.search();
            }
        });
    }
    parse(){
        return super.parse();
    }
    search(){
        return this.ajax.GET()
            .then((data)=>{
                let ulFragment = windsock.parse(JSON.parse(data)),
                    compiledUl = windsock.compile(ulFragment).children[0];
                //for now just wipe all existing results
                this.vdom.forEach((vdom)=>{
                    while(vdom.children[0].children.length){
                        vdom.children[0].children[0].remove();
                    }
                    //remove suggestions that are currently selected
                    //removeDuplicates(compiledUl, selectedPredictionsList, 'predictionId');
                    while(compiledUl.children.length){
                        //compiledUl.children[0].on('click', appendPredictionClone);
                        compiledUl.children[0].attributes.add('class','list-item');
                        vdom.children[0].append(compiledUl.children[0]);
                    }
                    // if(vdom.children[0].children.length){
                    //     if(!vdom.children[0].attributes.active){
                    //         vdom.children[0].attributes.add('active', 'true');
                    //     }
                    // }else{
                    //     vdom.children[0].attributes.delete('active');
                    // }
                });
            });
    }
}
