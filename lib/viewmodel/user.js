import Viewmodel from '../viewmodel';
import Ajax from '../ajax';

export default class User extends Viewmodel{
    constructor(selectors){
        super(selectors);
        // /user returns the current signed in user
        this.ajax = new Ajax({
            uri: '/user',
            params: {}
        });
        this.model = undefined;
    }
    update(){
        this.vdom.forEach((vdom)=>{
            vdom.children[0].class.add('updating');
        });
        return this.ajax.GET()
            .then((response)=>{
                //debugger;
                if(data === ''){
                    return;
                }
                this.model = response;
                this.vdom.forEach((vdom)=>{
                    vdom.children[0].class.remove('updating');
                    if(this.model === ''){
                        vdom.children[0].class.remove('signedin');
                    }else{
                        vdom.children[0].class.add('signedin');
                    }
                });
                return this.model;
            });
    }
    signedIn(){
        return this.model && this.model !== '';
    }
}
