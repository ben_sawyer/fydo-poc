import Viewmodel from '../viewmodel';
import toast from '../toast';

export default class Toast extends Viewmodel{
    constructor(){
        super();
    }
    parse(){
        super.parse();
        this.template[0].children[0].attributes.display = false;
        this.template[0].children[0].on('transitionend', function(){
            this.resolve && this.resolve();
            this.resolve = null;
        });
        return this;
    }
    compile(){
        super.compile();
        toast.vm = this;
        return this;
    }
    display(message){
        return new Promise((resolve, reject)=>{
            let elm = this.vdom[0].children[0];
            window.setTimeout(()=>{
                elm.resolve = resolve;
                elm.attributes.display = false;
            }, 3000);
            elm.text = message;
            elm.attributes.display = true;
        });
    }
}
Toast.selector = {
    toast: null
};
Toast.dependencies = [];
