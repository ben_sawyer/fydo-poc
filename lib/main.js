import app from './app';
import auth from './auth';
import root from './state/root';
import nav from './state/nav';
import signin from './state/signin';
import signup from './state/signup';
import Toast from './viewmodel/toast';
import Signin from './viewmodel/signin';
import Signup from './viewmodel/signup';
import Nav from './viewmodel/nav';

window.app = app;

app.auth = auth;

app.state.register('root', root),
app.state.register('nav', nav);
app.state.register('signin', signin);
app.state.register('signup', signup);

app.vm.add(Toast);
app.vm.add(Signin);
app.vm.add(Signup);
app.vm.add(Nav);

app.init();

// import Ajax from './ajax';
// import state from './state';
// import Map from './map';
//
// let geocoder = new google.maps.Geocoder(),
//     searchMap = new Map(map),
//     searchAjaxOptions = {
//         uri: '/search',
//         params: {
//             term: '',
//             ll: undefined
//         }
//     },
//     searchAjax = new Ajax(searchAjaxOptions),
//     searchDebounce,
//     locationAjaxOptions = {
//         uri: '/location',
//         params:{
//             input: ''
//         }
//     },
//     locationAjax = new Ajax(locationAjaxOptions),
//     locationDebounce;
//
// var predictionsList = windsock.compile(windsock.parse(predictions).children[0]);
// windsock.transclude(predictionsList, predictions); //hack for fragment return
//
// var selectedPredictionsList = windsock.compile(windsock.parse(selectedPredictions).children[0]);
// windsock.transclude(selectedPredictionsList, selectedPredictions);
//
// var searchResultsList = windsock.compile(windsock.parse(searchResults).children[0]);
// windsock.transclude(searchResultsList, searchResults); //hack for fragment return
//
// var selectedSearchResultsList = windsock.compile(windsock.parse(selectedSearchResults).children[0]);
// windsock.transclude(selectedSearchResultsList, selectedSearchResults);
//
// var boundsChangedListener;
//
// state.change.add(stateCheck);
//
// google.maps.event.addDomListener(window, 'load', init);
//
// function init(){
//     console.log('bootstraping app');
//     //setting options can only be done before initialization
//     searchMap.options.mapTypeControl = false;
//     searchMap.options.streetViewControl = false;
//     //when the app loads get position and init map
//     getCurrentPosition()
//         .then((pos)=>{
//             //set the zoom lvl and center of map
//             console.log('setting map zoom and center options');
//             searchMap.options.zoom = 13;
//             searchMap.options.center = new google.maps.LatLng(pos.lat, pos.lng);
//             console.log('setting location inputs values');
//             homeLocation.value = locationInput.value = searchLocation.value = pos.address;
//             console.log('initializing map');
//             return searchMap.init();
//         })
//         .then(()=>{
//             console.log('map initialized');
//             currentLocation.addEventListener('click', ()=>{
//                 getCurrentPosition()
//                     .then((pos)=>{
//                         //this does the same thing as selecting a prediction
//                         //force getting current again
//                         searchMap.center = {
//                             lat: pos.lat,
//                             lng: pos.lng
//                         };
//                         homeLocation.value = locationInput.value = searchLocation.value = pos.address;
//                     });
//                 selectedPredictionsList.children.forEach((child, i)=>{
//                     if(i === 0){
//                         child.class.add('selected');
//                     }else{
//                         child.class.remove('selected');
//                     }
//                 });
//                 state.current = 'home';
//             });
//             stateCheck();
//         })
//         .catch(()=>{
//             searchMap.init()
//                 .then(stateCheck);
//         });
// }
//
// homeSearch.addEventListener('click', ()=>{
//     state.current = 'search';
//     searchInput.focus();
// });
//
// homeLocation.addEventListener('click', ()=>{
//     state.current = 'location';
//     locationInput.focus();
// });
//
// searchLocation.addEventListener('click', ()=>{
//     state.current = 'location';
//     locationInput.focus();
// });
//
// searchInput.addEventListener('keyup', performSearch);
//
// // searchMap.listen('bounds_changed', ()=>{
// //     setPosition(searchMap.lat, searchMap.lng);
// //     performSearch();
// // });
//
// function stateCheck(){
//     console.log('checking state');
//     switch (state.previous) {
//         case 'location':
//             while(predictionsList.children.length){
//                 predictionsList.children[0].remove();
//             }
//             locationInput.value = '';
//             break;
//         case 'search':
//             while(searchResultsList.children.length){
//                 searchResultsList.children[0].remove();
//             }
//             searchInput.value = '';
//             break;
//         default:
//
//     }
//     switch(state.current){
//         case 'search':
//             performSearch();
//             break;
//         case 'location':
//             break;
//         default:
//             //do thangs
//     }
// }
//
// locationInput.addEventListener('keyup', ()=>{
//     locationAjaxOptions.params.input = locationInput.value;
//     if(locationDebounce){
//         clearTimeout(locationDebounce);
//     }
//     locationDebounce = setTimeout(()=>{
//         console.log('getting predictions');
//         locationAjax.GET()
//             .then((data)=>{
//                 let ulFragment = windsock.parse(JSON.parse(data)),
//                     compiled = windsock.compile(ulFragment);
//                 while(predictionsList.children.length){
//                     predictionsList.children[0].remove();
//                 }
//                 removeDuplicates(compiled.children[0], selectedPredictionsList, 'predictionId');
//                 while(compiled.children[0].children.length){
//                     compiled.children[0].children[0].on('click', appendPredictionClone);
//                     compiled.children[0].children[0].attributes.add('class','list-item');
//                     predictionsList.append(compiled.children[0].children[0]);
//                 }
//             });
//         locationDebounce = null;
//     }, 500);
// });
//
// function removeDuplicates(newList, existingList, attr){
//     if(!newList.length)return;
//     for(var i = 0; i < existingList.children.length; i++){
//         if(existingList.children[i].attributes[attr] === newList.children[0].attributes[attr]){
//             newList.children[0].remove();
//             return;
//         }
//     }
// }
//
// function performSearch(){
//     searchAjaxOptions.params.ll = searchMap.center.lat + ',' + searchMap.center.lng;
//     searchAjaxOptions.params.term = searchInput.value;
//     if(searchDebounce){
//         clearTimeout(searchDebounce);
//     }
//     searchDebounce = setTimeout(()=>{
//         console.log('performing search');
//         searchAjax.GET()
//             .then((data)=>{
//                 let ulFragment = windsock.parse(JSON.parse(data)),
//                     compiled = windsock.compile(ulFragment),
//                     marker,
//                     li;
//
//                 while(searchResultsList.children.length){
//                     searchResultsList.children[0].remove();
//                 }
//                 searchMap.clear();
//                 while(compiled.children[0].children.length){
//                     li = compiled.children[0].children[0];
//                     li.on('click', appendSearchResultClone);
//                     searchResultsList.append(li);
//                     marker = searchMap.marker(li.attributes.lat, li.attributes.long);
//                     marker.addListener('click', markerClick.bind(null, windsock.compile(li.clone(true)), marker));
//                 }
//             });
//         searchDebounce = null;
//     }, 500);
// }
//
// function appendPredictionClone(){
//     var clone = windsock.compile(this.clone(true));
//     clone.off();//remove previously cloned events
//     //also bind to child for remove
//     clone.on('click', ()=>{
//         selectPrediction(clone);
//     });
//     selectedPredictionsList.append(clone);
//     selectPrediction(clone);
// }
//
// function selectPrediction(p){
//     selectedPredictionsList.children.forEach((child)=>{
//         child.class.remove('selected');
//     });
//     p.class.add('selected');
//     updateMapCenterByAddress(p.text);
//     homeLocation.value = locationInput.value = searchLocation.value = p.text;
//     state.current = 'home';
// }
//
// function updateMapCenterByAddress(address){
//     getGeocode({'address': address})
//         .then((results)=>{
//             //update the map.center
//             searchMap.center = {
//                 lat: results[0].geometry.location.lat(),
//                 lng: results[0].geometry.location.lng()
//             };
//         })
//         .catch((err)=>{
//             console.warn('failed to get address lat/lng: ', err);
//         });
// }
//
// function getGeocode(data){
//     return new Promise((resolve, reject)=>{
//         geocoder.geocode(data, (results, status)=>{
//             if(status === google.maps.GeocoderStatus.OK){
//                 resolve(results);
//             }else{
//                 reject(status);
//             }
//         });
//     });
// }
//
// function appendSearchResultClone(){
//     var clone = windsock.compile(this.clone(true));
//     clone.on('click', updateSelectedSearchResult);
//     //remove previous selected location
//     selectedSearchResultsList.append(clone);
// }
//
// function updateSelectedSearchResult(){}
//
// function getPosition(){
//     return new Promise((resolve, reject) => {
//         navigator.geolocation.getCurrentPosition((pos)=>{
//             resolve({
//                 lat: pos.coords.latitude,
//                 lng: pos.coords.longitude
//             });
//         }, (err)=>{
//             reject(err);
//         },{
//           enableHighAccuracy: false,
//           timeout: 5000,
//           maximumAge: 0
//         });
//     });
// }
//
// function getCurrentPosition(){
//     var currentPosition;
//     console.log('getting current position');
//     return getPosition()
//         .then((pos)=>{
//             console.log('position retrieved', pos);
//             console.log('getting address');
//             currentPosition = pos;
//             return getGeocode({'location': pos});
//         })
//         .then((results)=>{
//             currentPosition.address = results.reduce((prev, current)=>{
//                 return current.types.indexOf('postal_code') !== -1 ? current.formatted_address.replace(', USA', '') : prev;
//             });
//             console.log('address retrieved', currentPosition.address);
//             return currentPosition;
//         });
// }
//
// function markerClick(li, marker){
//     selectedSearchResultsList.children.forEach((c)=>{c.remove();});
//     selectedSearchResultsList.append(li);
//     console.log('marker clicked', marker);
//     marker.icon.scale = 0.5;
// }
