let geo;
geo = {
    location: {
        position: undefined,
        address: undefined
    },
    config:{
        currentPosition:{
          enableHighAccuracy: false,
          timeout: 5000,
          maximumAge: 0
        }
    },
    geocoder: new google.maps.Geocoder(),
    getCurrentPosition: function(){
        return new Promise((resolve, reject) => {
            navigator.geolocation.getCurrentPosition((pos)=>{
                resolve(pos);
            }, (err)=>{
                reject(err);
            }, geo.config.currentPosition);
        });
    },
    getGeocode: function(data){
        return new Promise((resolve, reject)=>{
            geo.geocoder.geocode(data, (results, status)=>{
                if(status === google.maps.GeocoderStatus.OK){
                    resolve(results);
                }else{
                    reject(status);
                }
            });
        });
    },
    getCurrent: function(){
        console.log('getting current location');
        return geo.getCurrentPosition()
            .then((position)=>{
                geo.location.position = position;
                return geo.getGeocode({
                    'location': {
                        lat: position.coords.latitude,
                        lng: position.coords.longitude
                    }
                });
            })
            .then((results)=>{
                geo.location.address = results.reduce((prev, current)=>{
                    return current.types.indexOf('postal_code') !== -1 ? current : prev;
                });
                return geo.location;
            });
    }
};
export default geo;
