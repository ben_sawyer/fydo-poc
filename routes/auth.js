var config = require('../config'),
    bodyParser = require('body-parser'),
    jwt = require('jsonwebtoken'),
    passport = require('passport'),
    FacebookStrategy = require('passport-facebook').Strategy,
    GoogleStrategy = require('passport-google-oauth').OAuth2Strategy,
    express = require('express'),
    router = express.Router(),
    models = require('../models');

var urlencodedParser = bodyParser.urlencoded({extended: false}),
    jsonParser = bodyParser.json();

// Facebook will send back the token and profile after approval
passport.use(new FacebookStrategy(config[process.env.PROVIDER_CONFIG].facebook, OAuthStrategyVerify));
passport.use(new GoogleStrategy(config[process.env.PROVIDER_CONFIG].google, OAuthStrategyVerify));

function OAuthStrategyVerify(token, refreshToken, profile, done){
    return models.Provider.findOne({
            where: {
                type: profile.provider,
                profileId: profile.id
            }
        })
        .then(function(provider){
            if(provider === null){
                //if one doesn't exist find or create a user
                return models.User.findOrCreate({
                    where: {
                        email: profile.emails[0].value.toLowerCase()
                    }
                })
                .spread(function(user, created){
                    return models.Provider.create({
                        type: profile.provider,
                        profileId: profile.id,
                        token: token,
                        email: profile.emails[0].value.toLowerCase(),
                        name: profile.displayName,
                        UserId: user.get({plain: true}).id
                    })
                    .then(function(){
                        done(null, user.get({plain:true}));
                    });
                })
            }else{
                //update provider
                return provider.update({
                    token: token,
                    email: profile.emails[0].value.toLowerCase()
                },{
                    where: {
                        type: profile.provider,
                        profileId: profile.id
                    }
                }).then(function(){
                    //if a provider exists so does a user
                    return models.User.findOne({
                        where: {
                            id: provider.get({plain: true}).UserId
                        }
                    })
                    .then(function(user){
                        done(null, user.get({plain:true}));
                    });
                });
            }
        });
}

router.use(passport.initialize());

router.get('/facebook', urlencodedParser, jsonParser, passport.authenticate('facebook', {
    session: false,
    scope: 'email'
}));

router.get('/facebook/callback', urlencodedParser, passport.authenticate('facebook', {
    session: false,
    failureRedirect: '/#signin'
}),function(req, res){
    res.redirect('/?token=' + jwt.sign({email: req.user.email}, process.env.TOKEN_SECRET, {expiresIn: 300}));
});

router.get('/google', urlencodedParser, jsonParser, passport.authenticate('google', {
    session: false,
    scope: [
       'https://www.googleapis.com/auth/plus.login',
       'https://www.googleapis.com/auth/plus.profile.emails.read'
   ]
}));

router.get('/google/callback', urlencodedParser, passport.authenticate('google', {
    session: false,
    failureRedirect: '/#signin'
}),function(req, res){
    res.redirect('/?token=' + jwt.sign({email: req.user.email}, process.env.TOKEN_SECRET, {expiresIn: 300}));
});

module.exports = router;
