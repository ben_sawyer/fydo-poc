var bodyParser = require('body-parser'),
    jwt = require('jsonwebtoken'),
    express = require('express'),
    models = require('../../models'),
    user = require('./user'),
    location = require('./location'),
    router = express.Router();

var urlencodedParser = bodyParser.urlencoded({extended: false}),
    jsonParser = bodyParser.json();

router.post('/auth', urlencodedParser, jsonParser, function(req, res){
    return models.User.findOne({
        where: {
            email: req.body.email
        }
    })
    .then(function(user){
        if(user === null){
            return res.status(401)
                .send({
                    success: false,
                    message: 'Authentication failed. User not found.'
                });
        }else{
            if(user.password !== req.body.password){
                return res.status(401)
                    .send({
                        success: false,
                        message: 'Authentication failed. Wrong password.'
                    });
            }else{
                return res.json({
                    token: jwt.sign({email: user.get({plain:true}).email}, process.env.TOKEN_SECRET, {expiresIn: 300})
                });
            }
        }
    })
    .catch(function(err){
        return res.status(401)
            .send({
                success: false,
                message: 'Failed to authenticate with credentials.'
            });
    });
});

router.use('/user', user.exposed);

router.use(urlencodedParser, jsonParser, function(req, res, next){
    var token = req.body.token || req.query.token || req.headers['x-access-token'];
    if(token){
        jwt.verify(token, process.env.TOKEN_SECRET, function(err, decoded){
            if(err){
                return res.status(401)
                    .send({
                        success: false,
                        message: 'Failed to authenticate token.'
                    });
            }
            req.decoded = decoded;
            next();
        });
    }else{
        return res.status(401)
            .send({
                success: false,
                message: 'Missing token.'
            });
    }
});

router.use('/user', user.auth);
router.use('/location', location);

module.exports = router;
