var bodyParser = require('body-parser'),
    express = require('express'),
    GoogleLocations = require('google-locations'),
    router = express.Router();

var urlencodedParser = bodyParser.urlencoded({extended: false}),
    locations = new GoogleLocations('AIzaSyDW-BOkJ3qalg0TwwNh40kMnK07kQFH_vc');

router.get('/autocomplete', urlencodedParser, function(req, res){
    locations.autocomplete({
            input: req.query.input,
            type:'(cities)'
        }, function(err, data){
        if(err){
            return res.status(500)
                .send({
                    success: false,
                    message: err
                });
        }
        res.json(data);
    });
})

module.exports = router;
