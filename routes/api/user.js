var bodyParser = require('body-parser'),
    express = require('express'),
    models = require('../../models'),
    auth = express.Router(),
    exposed = express.Router();

var urlencodedParser = bodyParser.urlencoded({extended: false}),
    jsonParser = bodyParser.json();

exposed.post('/', urlencodedParser, jsonParser, function(req, res){
    //move this to validation middleware
    var validate = ['email', 'password', 'confirmPassword'];
    if(!req.body){
        return res.status(400)
            .send({
                success: false,
                message: 'Missing body.'
            });
    }
    for(var i=0; i < validate.length; i++){
        if(!req.body[validate[i]]){
            return res.status(400)
                .send({
                    success: false,
                    message: 'Missing required value for ' + validate[i] + '.'
                });
        }
    }
    if(req.body.email.match(/(\w[-._\w]*\w@\w[-._\w]*\w\.\w{2,3})/g) === null){
        return res.status(400)
            .send({
                success: false,
                message: 'Invalid Email.'
            });
    }
    if(req.body.password !== req.body.confirmPassword){
        return res.status(400)
            .send({
                success: false,
                message: 'Passwords do not match.'
            });
    }
    models.User.findOrCreate({
        where:{
            email: req.body.email
        },
        defaults:{
            email: req.body.email,
            password: req.body.password
        }
    })
    .spread(function(user){
        res.json(user);
    })
    .catch(function(err){
        //check errors[] for types and respond with proper code
        return res.status(500)
            .send({
                success: false,
                message: 'Failed to create user'
            });
    });
});

auth.delete('/:id', function(req, res){
    models.User.destroy({
        where: {
            id: req.params.id
        }
    })
    .then(function(result){
        res.json(result);
    });
});

auth.get('/', function(req, res){
    models.User.findAll()
    .then(function(users){
        res.json(users);
    });
});

exports.exposed = exposed;
exports.auth = auth;
