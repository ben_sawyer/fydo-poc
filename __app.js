var path = require('path'),
    errorHandler = require('errorhandler'),
    morgan = require('morgan'),
    http = require('http'),
    pg = require('pg'),
    session = require('express-session'),
    pgSession = require('connect-pg-simple')(session),
    passport = require('passport'),
    passportLocal = require('passport-local'),
    bodyParser = require('body-parser'),
    signin = require('./routes/signin'),
    user = require('./routes/user'),
    search = require('./routes/search'),
    express = require('express');

// TODO rename routes to viewmodels

var app = express(),
    init = [],
    connectionString = process.env.CONNECTION_STRING || 'postgres://postgres@postgresql.service.consul/fydo',
    sess = {
        store: new pgSession({
            pg: pg,
            conString: connectionString
        }),
        secret: process.env.COOKIE_SECRET || 'derp',
        resave: false,
        saveUninitialized: false, //something weird about passport.js here
        cookie: { maxAge: 30 * 24 * 60 * 60 * 1000 } //30days
    },
    urlencodedParser = bodyParser.urlencoded({extended: false}),
    jsonParser = bodyParser.json();

app.set('port', process.env.PORT || 3000);
app.use(express.static(path.join(__dirname, 'static')));//static before sessions

if(app.get('env') === 'production'){
    app.set('trust proxy', 1); // trust first proxy
    sess.cookie.secure = true; // serve secure cookies
}

app.use(session(sess));
app.use(passport.initialize());//passport middlewarezzzz
app.use(passport.session());//persist login sessions

// app.get('/', function(req, res, next) {
//   var sess = req.session
//   if (sess.views) {
//     sess.views++
//     res.setHeader('Content-Type', 'text/html')
//     res.write('<p>views: ' + sess.views + '</p>')
//     res.write('<p>expires in: ' + (sess.cookie.maxAge / 1000) + 's</p>')
//     res.end()
//   } else {
//     sess.views = 1
//     res.end('welcome to the session demo. refresh!')
//   }
// });

passport.use(new passportLocal.Strategy({
        usernameField: 'email',
        passwordField: 'password'
    },
    passportLocalVerifyCallback));//passport strategyzzzz
passport.serializeUser(passportSerializeUser);
passport.deserializeUser(passportDeserializeUser);

function passportLocalVerifyCallback(username, password, done){
    console.log('Verifying user credentials.', arguments);
    pg.connect(connectionString, function(connectionErr, client, connectionDone){
        if(connectionErr){
            res.status(500)
                .send('Failed to connect to db');
            done(null, false, {message:'Failed to connect to db'});
            return connectionDone();
        }else{
            client.query("SELECT * FROM public.user WHERE email = '" + username + "';", function(userExistsErr, userExistsResult){
                if(userExistsErr){
                    done(null, false, {message: 'Failed to find user.'});
                }else if(!userExistsResult.rows.length){
                    done(null, false, {message:'Incorrect username.'});
                }else if(userExistsResult.rows[0].password !== password){
                    console.log('password missmatch: ' + password + '!==' + userExistsResult.rows[0].password);
                    done(null, false, {message:'Incorrect password.'});
                }else{
                    console.log('user exists!', userExistsResult.rows[0]);
                    done(null, userExistsResult.rows[0]);
                }
                return connectionDone();
            });
        }
    });
}

function passportSerializeUser(user, done){
    console.log('Serializing user for passport.');
    done(null, user);
}

function passportDeserializeUser(user, done){
    //user is what serialize calls done with
    //lookup user and return here
    console.log('Deserializing user for passport.');
    console.log(user);
    done(null, user);
}

// app.post('/signup', urlencodedParser, connect.signup.POST);

app.post('/signin', jsonParser, passport.authenticate('local'), signin.POST);


app.get('/signout', function(req, res){
    console.log(req.user);
    req.session.destroy(function(){
        res.redirect('/');
    });
});

app.get('/user/:id', function(req, res, next){
    passport.authenticate('local', function(err, user, info) {
        console.log(err);
        if (!user) {
            console.log('missing user');
        }
        console.log(user);
        console.log(info);
        next();
    })(req, res, next);
}, function(req, res){
    console.log('user:', req.user);
    console.log('session:', req.session);
    console.log('id:', req.params.id);
    res.end();
});

app.get('/search', search.GET);

switch(app.get('env')){
    case 'production':
        console.log('Application using production configuration.');
        app.use(morgan('common'));
        break;
    case 'development':
    default:
        console.log('Application using development configuration.');
        app.use(errorHandler()); //should be added after loading routes
        app.use(morgan('dev'));
        break;
}

init.push(function(){
    return new Promise(function(resolve, reject){
        console.log('Creating postgresql connection pool.')
        app.set('pg', pg);
        app.set('connectionString', connectionString);
        pg.connect(connectionString, function(e, client, done){
            done();
            if(e){
                console.log('Exception initializing postgresql connection pool.');
                console.log(e);
                return reject(e);
            }
            console.log('Postgresql connection pool initialization successful.');
            resolve();
        });
    });
});

init.push(function(){
    return new Promise(function(resolve, reject){
        console.log('Creating express server.');
        var server = http.createServer(app);
        server.on('error', function(error){
            console.log('Express server error.');
            reject(error);
        });
        server.listen(app.get('port'), function(){
            console.log('Express server listening on port ' + app.get('port') + '.');
            resolve(server);
        });
    });
});

init.reduce(function(p, current){
        return p.then(current);
    }, Promise.resolve())
    .then(function(){
        console.log('Application initialization successful.');
    }, function(e){
        console.log('Exception initializing application.');
        console.log(e);
    });
