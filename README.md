#Fydo

##Server and client repository

###Installation
install otto

###Development
```bash
otto compile
otto dev

# for OAUTH callbacks
otto dev address

# add an entry in your hosts file
sudo vi /etc/hosts
100.124.131.89    local.fydo.us

otto dev ssh
npm install

# setup postgresql
npm run postgres

# start the server
npm run dev
```

Then in a separate terminal
```bash
npm run build:watch
```

local.fydo.us:8888

###Production
