var fs = require('fs'),
    path = require('path'),
    Sequelize = require('sequelize');

// Sequelize will setup a connection pool on initialization.
// Create one instance per database.
exports.sequelize = new Sequelize(process.env.POSTGRES_URI);

fs.readdirSync(__dirname)
    .filter(function(filename){
        return filename.match(/\.js$/i) && (filename !== "index.js");
    })
    .map(function(filename){
        // Imported models are cached, so multiple calls to import
        // with the same path will not load the file multiple times.
        var model = exports.sequelize.import(path.join(__dirname, filename));
        return exports[model.name] = model;
    })
    .forEach(function(model){
        model.associate && model.associate(exports);
    });
