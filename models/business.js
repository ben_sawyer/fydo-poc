module.exports = function(sequelize, DataTypes){
    return sequelize.define('Business',{
        name: {
            type: DataTypes.STRING
        },
        phone: {
            type: DataTypes.STRING
        }
    });
};
