module.exports = function(sequelize, DataTypes){
    var User;
    User = sequelize.define('User',{
        email: {
            type: DataTypes.STRING,
            unique: true
        },
        password: DataTypes.STRING,
        verified: {
            defaultValue: false,
            type: DataTypes.BOOLEAN
        }
    }, {
        classMethods: {
            associate: function(models){
                User.hasMany(models.Provider);
            }
        }
    });
    return User;
};
