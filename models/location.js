module.exports = function(sequelize, DataTypes){
    return sequelize.define('Business',{
        address: {
            type: DataTypes.STRING
        },
        city:{
            type: DataTypes.STRING
        },
        state:{
            type: DataTypes.STRING
        },
        country:{
            type: DataTypes.STRING
        },
        zip:{
            type: DataTypes.STRING
        },
        lat:{
            type: DataTypes.DECIMAL
        },
        lng:{
            type: DataTypes.DECIMAL
        }
    });
};
