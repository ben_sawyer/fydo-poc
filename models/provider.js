module.exports = function(sequelize, DataTypes){
    var Provider;
    Provider = sequelize.define('Provider', {
        type: {
            type: DataTypes.ENUM,
            values: ['facebook', 'twitter', 'google']
        },
        profileId: DataTypes.STRING,
        token: DataTypes.STRING,
        email: DataTypes.STRING,
        username: DataTypes.STRING,
        name: DataTypes.STRING
    },{
        classMethods: {
            associate: function(models){
                Provider.belongsTo(models.User, {
                    onDelete: "CASCADE",
                    foreignKey: {
                        allowNull: false
                    }
                });
            }
        }
    });
    return Provider;
};
