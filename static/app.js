(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _auth = require('./auth');

var _auth2 = _interopRequireDefault(_auth);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Ajax = function () {
    function Ajax(options) {
        _classCallCheck(this, Ajax);

        this.options = options;
    }

    _createClass(Ajax, [{
        key: 'GET',
        value: function GET() {
            return Ajax.request(this.url);
        }
    }, {
        key: 'POST',
        value: function POST(data) {
            return Ajax.request(this.url, 'POST', data);
        }
    }, {
        key: 'PUT',
        value: function PUT(data) {
            return Ajax.request(this.url, 'PUT', data);
        }
    }, {
        key: 'DELETE',
        value: function DELETE() {
            return Ajax.request(this.url, 'DELETE');
        }
    }, {
        key: 'url',
        get: function get() {
            return this.options.uri + Ajax.params(this.options.params);
        }
    }], [{
        key: 'request',
        value: function request(url) {
            var method = arguments.length <= 1 || arguments[1] === undefined ? 'GET' : arguments[1];
            var data = arguments.length <= 2 || arguments[2] === undefined ? {} : arguments[2];

            return new Promise(function (resolve, reject) {
                var client = new XMLHttpRequest();
                client.open(method, url, true);
                if (method === 'POST' || method === 'PUT') {
                    client.setRequestHeader('Content-Type', 'application/json');
                }
                client.send(JSON.stringify(data));
                client.onload = function () {
                    if (this.status >= 200 && this.status < 300) {
                        resolve(JSON.parse(this.response));
                    } else {
                        reject(JSON.parse(this.response), this);
                        if (this.status === 401) {
                            _auth2.default.token = null;
                        }
                    }
                };
                client.onerror = function () {
                    reject(this.statusText);
                };
            });
        }
    }, {
        key: 'params',
        value: function params() {
            var _params = arguments.length <= 0 || arguments[0] === undefined ? {} : arguments[0];

            return Object.keys(_params).map(function (key, i) {
                var param = i === 0 ? '?' : '&';
                param += key;
                if (_params[key]) param += '=' + _params[key];
                return param;
            }).join('');
        }
    }]);

    return Ajax;
}();

exports.default = Ajax;

},{"./auth":3}],2:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _state = require('./state');

var _state2 = _interopRequireDefault(_state);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var app = undefined,
    states = {
    root: undefined
},
    viewmodels = [];
app = {
    init: function init() {
        app.state.init();
        app.vm.init();
    },
    destroy: function destroy() {
        for (var name in states) {
            states[name].destroy();
        }
        _state2.default.change.remove();
    },
    state: {
        init: function init() {
            _state2.default.ready.add(function (current, results) {
                var currentState = app.state.resolve(current);
                currentState && currentState.ready.dispatch(results, app);
            });
            _state2.default.load.add(function (current) {
                var initialState = app.state.resolve(current);
                if (initialState) {
                    _state2.default.resolve(initialState.load.dispatch(app));
                }
            });
            _state2.default.change.add(function (current, previous) {
                var currentState = app.state.resolve(current),
                    previousState = app.state.resolve(previous);
                previousState && previousState.leave.dispatch(current, app);
                if (currentState) {
                    _state2.default.resolve(currentState.enter.dispatch(previous, app));
                }
            });
            //state load get session
            //and any request that requires a session cookie needs to handle failures
        },
        register: function register(name, config) {
            if (!name) {
                throw new Error('failed to register state, missing name');
            }
            return registerState(name, config);
        },
        resolve: function resolve(name) {
            return name.length ? states[name] : states.root;
        },
        go: function go(request) {
            if (states[request]) {
                _state2.default.current = request;
            }
        }
    },
    vm: {
        init: function init() {
            viewmodels.forEach(function (Viewmodel) {
                var vm = new Viewmodel();
                vm.render();
            });
        },
        add: function add(Viewmodel) {
            viewmodels.push(Viewmodel);
        }
    }
};
function registerState(stateName, config) {
    states[stateName] = _state2.default.create();
    Object.keys(config).forEach(function (signal) {
        if (!states[stateName][signal]) {
            throw new Error('failed to register state, signal ' + signal + ' does not exist');
        }
        if (Object.prototype.toString.call(config[signal]) === '[object Function]') {
            states[stateName][signal].add(config[signal]);
        } else {
            for (var key in config[signal]) {
                states[stateName][signal].add(config[signal][key]);
            }
        }
    });
    return states[name];
}
exports.default = app;

},{"./state":6}],3:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _signal = require('./signal');

var _signal2 = _interopRequireDefault(_signal);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var auth = undefined,
    token = null;
auth = {
    update: new _signal2.default()
};
Object.defineProperties(auth, {
    token: {
        get: function get() {
            return token === null ? token = JSON.parse(window.localStorage.getItem('auth_token')) : token;
        },
        set: function set(value) {
            if (token === value) return;
            token = value;
            if (token === null) {
                window.localStorage.removeItem('auth_token');
            } else {
                window.localStorage.setItem('auth_token', JSON.stringify(value));
            }
            this.update(token);
        }
    }
});
exports.default = auth;

},{"./signal":5}],4:[function(require,module,exports){
'use strict';

var _app = require('./app');

var _app2 = _interopRequireDefault(_app);

var _auth = require('./auth');

var _auth2 = _interopRequireDefault(_auth);

var _root = require('./state/root');

var _root2 = _interopRequireDefault(_root);

var _nav = require('./state/nav');

var _nav2 = _interopRequireDefault(_nav);

var _signin = require('./state/signin');

var _signin2 = _interopRequireDefault(_signin);

var _signup = require('./state/signup');

var _signup2 = _interopRequireDefault(_signup);

var _toast = require('./viewmodel/toast');

var _toast2 = _interopRequireDefault(_toast);

var _signin3 = require('./viewmodel/signin');

var _signin4 = _interopRequireDefault(_signin3);

var _signup3 = require('./viewmodel/signup');

var _signup4 = _interopRequireDefault(_signup3);

var _nav3 = require('./viewmodel/nav');

var _nav4 = _interopRequireDefault(_nav3);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

window.app = _app2.default;

_app2.default.auth = _auth2.default;

_app2.default.state.register('root', _root2.default), _app2.default.state.register('nav', _nav2.default);
_app2.default.state.register('signin', _signin2.default);
_app2.default.state.register('signup', _signup2.default);

_app2.default.vm.add(_toast2.default);
_app2.default.vm.add(_signin4.default);
_app2.default.vm.add(_signup4.default);
_app2.default.vm.add(_nav4.default);

_app2.default.init();

// import Ajax from './ajax';
// import state from './state';
// import Map from './map';
//
// let geocoder = new google.maps.Geocoder(),
//     searchMap = new Map(map),
//     searchAjaxOptions = {
//         uri: '/search',
//         params: {
//             term: '',
//             ll: undefined
//         }
//     },
//     searchAjax = new Ajax(searchAjaxOptions),
//     searchDebounce,
//     locationAjaxOptions = {
//         uri: '/location',
//         params:{
//             input: ''
//         }
//     },
//     locationAjax = new Ajax(locationAjaxOptions),
//     locationDebounce;
//
// var predictionsList = windsock.compile(windsock.parse(predictions).children[0]);
// windsock.transclude(predictionsList, predictions); //hack for fragment return
//
// var selectedPredictionsList = windsock.compile(windsock.parse(selectedPredictions).children[0]);
// windsock.transclude(selectedPredictionsList, selectedPredictions);
//
// var searchResultsList = windsock.compile(windsock.parse(searchResults).children[0]);
// windsock.transclude(searchResultsList, searchResults); //hack for fragment return
//
// var selectedSearchResultsList = windsock.compile(windsock.parse(selectedSearchResults).children[0]);
// windsock.transclude(selectedSearchResultsList, selectedSearchResults);
//
// var boundsChangedListener;
//
// state.change.add(stateCheck);
//
// google.maps.event.addDomListener(window, 'load', init);
//
// function init(){
//     console.log('bootstraping app');
//     //setting options can only be done before initialization
//     searchMap.options.mapTypeControl = false;
//     searchMap.options.streetViewControl = false;
//     //when the app loads get position and init map
//     getCurrentPosition()
//         .then((pos)=>{
//             //set the zoom lvl and center of map
//             console.log('setting map zoom and center options');
//             searchMap.options.zoom = 13;
//             searchMap.options.center = new google.maps.LatLng(pos.lat, pos.lng);
//             console.log('setting location inputs values');
//             homeLocation.value = locationInput.value = searchLocation.value = pos.address;
//             console.log('initializing map');
//             return searchMap.init();
//         })
//         .then(()=>{
//             console.log('map initialized');
//             currentLocation.addEventListener('click', ()=>{
//                 getCurrentPosition()
//                     .then((pos)=>{
//                         //this does the same thing as selecting a prediction
//                         //force getting current again
//                         searchMap.center = {
//                             lat: pos.lat,
//                             lng: pos.lng
//                         };
//                         homeLocation.value = locationInput.value = searchLocation.value = pos.address;
//                     });
//                 selectedPredictionsList.children.forEach((child, i)=>{
//                     if(i === 0){
//                         child.class.add('selected');
//                     }else{
//                         child.class.remove('selected');
//                     }
//                 });
//                 state.current = 'home';
//             });
//             stateCheck();
//         })
//         .catch(()=>{
//             searchMap.init()
//                 .then(stateCheck);
//         });
// }
//
// homeSearch.addEventListener('click', ()=>{
//     state.current = 'search';
//     searchInput.focus();
// });
//
// homeLocation.addEventListener('click', ()=>{
//     state.current = 'location';
//     locationInput.focus();
// });
//
// searchLocation.addEventListener('click', ()=>{
//     state.current = 'location';
//     locationInput.focus();
// });
//
// searchInput.addEventListener('keyup', performSearch);
//
// // searchMap.listen('bounds_changed', ()=>{
// //     setPosition(searchMap.lat, searchMap.lng);
// //     performSearch();
// // });
//
// function stateCheck(){
//     console.log('checking state');
//     switch (state.previous) {
//         case 'location':
//             while(predictionsList.children.length){
//                 predictionsList.children[0].remove();
//             }
//             locationInput.value = '';
//             break;
//         case 'search':
//             while(searchResultsList.children.length){
//                 searchResultsList.children[0].remove();
//             }
//             searchInput.value = '';
//             break;
//         default:
//
//     }
//     switch(state.current){
//         case 'search':
//             performSearch();
//             break;
//         case 'location':
//             break;
//         default:
//             //do thangs
//     }
// }
//
// locationInput.addEventListener('keyup', ()=>{
//     locationAjaxOptions.params.input = locationInput.value;
//     if(locationDebounce){
//         clearTimeout(locationDebounce);
//     }
//     locationDebounce = setTimeout(()=>{
//         console.log('getting predictions');
//         locationAjax.GET()
//             .then((data)=>{
//                 let ulFragment = windsock.parse(JSON.parse(data)),
//                     compiled = windsock.compile(ulFragment);
//                 while(predictionsList.children.length){
//                     predictionsList.children[0].remove();
//                 }
//                 removeDuplicates(compiled.children[0], selectedPredictionsList, 'predictionId');
//                 while(compiled.children[0].children.length){
//                     compiled.children[0].children[0].on('click', appendPredictionClone);
//                     compiled.children[0].children[0].attributes.add('class','list-item');
//                     predictionsList.append(compiled.children[0].children[0]);
//                 }
//             });
//         locationDebounce = null;
//     }, 500);
// });
//
// function removeDuplicates(newList, existingList, attr){
//     if(!newList.length)return;
//     for(var i = 0; i < existingList.children.length; i++){
//         if(existingList.children[i].attributes[attr] === newList.children[0].attributes[attr]){
//             newList.children[0].remove();
//             return;
//         }
//     }
// }
//
// function performSearch(){
//     searchAjaxOptions.params.ll = searchMap.center.lat + ',' + searchMap.center.lng;
//     searchAjaxOptions.params.term = searchInput.value;
//     if(searchDebounce){
//         clearTimeout(searchDebounce);
//     }
//     searchDebounce = setTimeout(()=>{
//         console.log('performing search');
//         searchAjax.GET()
//             .then((data)=>{
//                 let ulFragment = windsock.parse(JSON.parse(data)),
//                     compiled = windsock.compile(ulFragment),
//                     marker,
//                     li;
//
//                 while(searchResultsList.children.length){
//                     searchResultsList.children[0].remove();
//                 }
//                 searchMap.clear();
//                 while(compiled.children[0].children.length){
//                     li = compiled.children[0].children[0];
//                     li.on('click', appendSearchResultClone);
//                     searchResultsList.append(li);
//                     marker = searchMap.marker(li.attributes.lat, li.attributes.long);
//                     marker.addListener('click', markerClick.bind(null, windsock.compile(li.clone(true)), marker));
//                 }
//             });
//         searchDebounce = null;
//     }, 500);
// }
//
// function appendPredictionClone(){
//     var clone = windsock.compile(this.clone(true));
//     clone.off();//remove previously cloned events
//     //also bind to child for remove
//     clone.on('click', ()=>{
//         selectPrediction(clone);
//     });
//     selectedPredictionsList.append(clone);
//     selectPrediction(clone);
// }
//
// function selectPrediction(p){
//     selectedPredictionsList.children.forEach((child)=>{
//         child.class.remove('selected');
//     });
//     p.class.add('selected');
//     updateMapCenterByAddress(p.text);
//     homeLocation.value = locationInput.value = searchLocation.value = p.text;
//     state.current = 'home';
// }
//
// function updateMapCenterByAddress(address){
//     getGeocode({'address': address})
//         .then((results)=>{
//             //update the map.center
//             searchMap.center = {
//                 lat: results[0].geometry.location.lat(),
//                 lng: results[0].geometry.location.lng()
//             };
//         })
//         .catch((err)=>{
//             console.warn('failed to get address lat/lng: ', err);
//         });
// }
//
// function getGeocode(data){
//     return new Promise((resolve, reject)=>{
//         geocoder.geocode(data, (results, status)=>{
//             if(status === google.maps.GeocoderStatus.OK){
//                 resolve(results);
//             }else{
//                 reject(status);
//             }
//         });
//     });
// }
//
// function appendSearchResultClone(){
//     var clone = windsock.compile(this.clone(true));
//     clone.on('click', updateSelectedSearchResult);
//     //remove previous selected location
//     selectedSearchResultsList.append(clone);
// }
//
// function updateSelectedSearchResult(){}
//
// function getPosition(){
//     return new Promise((resolve, reject) => {
//         navigator.geolocation.getCurrentPosition((pos)=>{
//             resolve({
//                 lat: pos.coords.latitude,
//                 lng: pos.coords.longitude
//             });
//         }, (err)=>{
//             reject(err);
//         },{
//           enableHighAccuracy: false,
//           timeout: 5000,
//           maximumAge: 0
//         });
//     });
// }
//
// function getCurrentPosition(){
//     var currentPosition;
//     console.log('getting current position');
//     return getPosition()
//         .then((pos)=>{
//             console.log('position retrieved', pos);
//             console.log('getting address');
//             currentPosition = pos;
//             return getGeocode({'location': pos});
//         })
//         .then((results)=>{
//             currentPosition.address = results.reduce((prev, current)=>{
//                 return current.types.indexOf('postal_code') !== -1 ? current.formatted_address.replace(', USA', '') : prev;
//             });
//             console.log('address retrieved', currentPosition.address);
//             return currentPosition;
//         });
// }
//
// function markerClick(li, marker){
//     selectedSearchResultsList.children.forEach((c)=>{c.remove();});
//     selectedSearchResultsList.append(li);
//     console.log('marker clicked', marker);
//     marker.icon.scale = 0.5;
// }

},{"./app":2,"./auth":3,"./state/nav":7,"./state/root":8,"./state/signin":9,"./state/signup":10,"./viewmodel/nav":14,"./viewmodel/signin":15,"./viewmodel/signup":16,"./viewmodel/toast":17}],5:[function(require,module,exports){
"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

Object.defineProperty(exports, "__esModule", {
    value: true
});

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Signal = function () {
    function Signal() {
        _classCallCheck(this, Signal);

        this.listeners = [];
    }

    _createClass(Signal, [{
        key: "add",
        value: function add(listener) {
            this.listeners.push(listener);
        }
    }, {
        key: "remove",
        value: function remove(listener) {
            if (listener) {
                return this.listeners.splice(this.listeners.indexOf(listener), 1);
            }
            this.listeners = [];
        }
    }, {
        key: "dispatch",
        value: function dispatch() {
            var _this = this;

            var args = Array.prototype.slice.call(arguments);
            return this.listeners.map(function (listener) {
                return listener.apply(_this, args);
            });
        }
    }]);

    return Signal;
}();

exports.default = Signal;

},{}],6:[function(require,module,exports){
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _signal = require('./signal');

var _signal2 = _interopRequireDefault(_signal);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var state = undefined,
    current = undefined;
state = {
    create: function create() {
        return new State();
    },
    previous: '',
    change: new _signal2.default(),
    load: new _signal2.default(),
    ready: new _signal2.default(),
    resolve: function resolve(promise) {
        if (Object.prototype.toString.call(promise) === '[object Array]') {
            state.map = state.map.concat(promise);
        } else {
            state.map.unshift(promise);
        }
    }
};

var State = function () {
    function State() {
        _classCallCheck(this, State);

        this.enter = new _signal2.default();
        this.leave = new _signal2.default();
        this.load = new _signal2.default();
        this.ready = new _signal2.default();
    }

    _createClass(State, [{
        key: 'destroy',
        value: function destroy() {
            this.enter.remove();
            this.leave.remove();
            this.load.remove();
            this.ready.remove();
        }
    }]);

    return State;
}();

Object.defineProperties(state, {
    current: {
        get: function get() {
            return current;
        },
        set: function set(value) {
            location.hash = value;
        },
        enumerable: true
    }
});
window.addEventListener('hashchange', function () {
    state.map = [];
    state.previous = state.current;
    current = location.hash.substr(1);
    state.change.dispatch(current, state.previous);
    //state map is updated individually by each listener of of state.load
    Promise.all(state.map).then(function (results) {
        state.ready.dispatch(current, results);
    });
});
window.addEventListener('load', function () {
    state.map = [];
    current = location.hash.substr(1) || '';
    state.load.dispatch(current);
    //state map is updated individually by each listener of of state.load
    //by invoking state.resolve(promise|promises)
    Promise.all(state.map).then(function (results) {
        state.ready.dispatch(current, results);
    });
});
exports.default = state;

},{"./signal":5}],7:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _state = require('../state');

var _state2 = _interopRequireDefault(_state);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var nav = undefined;
nav = {
    ready: function ready(results, app) {
        //console.log('nav:ready', results);
    },
    enter: function enter() {
        main.addEventListener('click', mainClick);
    },
    leave: function leave(current) {
        main.removeEventListener('click', mainClick);
    },
    load: function load(app) {
        main.addEventListener('click', mainClick);
    }
};
function mainClick() {
    _state2.default.current = _state2.default.previous;
}
exports.default = nav;

},{"../state":6}],8:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _ajax = require('../ajax');

var _ajax2 = _interopRequireDefault(_ajax);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var root = undefined;
root = {
    ready: function ready(results, app) {
        //console.log('root:ready', results);
    },
    enter: function enter(previous) {
        //console.log('root:enter, previous state was ' + previous);
    },
    leave: function leave(current) {
        //console.log('root:leave, current state is ' + current);
    },
    load: function load(app) {
        //console.log('root:load');
    }
};
exports.default = root;

},{"../ajax":1}],9:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
var signin = undefined;
signin = {
    ready: function ready(results, app) {
        //console.log('signin:ready', results);
    },
    load: function load(app) {
        //console.log('signin:load');
    }
};
exports.default = signin;

},{}],10:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
var signup = undefined;
signup = {
    ready: function ready(results, app) {
        //console.log('signup:ready', results);
    },
    load: function load(app) {
        //console.log('signup:load');
    }
};
exports.default = signup;

},{}],11:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
var toast = undefined,
    queue = [];
toast = {
    vm: undefined,
    simple: function simple(message) {
        if (queue.push(message) <= 1) display();
    },
    error: function error() {
        //have a different queue for errors
    }
};
function display() {
    if (queue.length) toast.vm.display(queue.shift()).then(display);
}
exports.default = toast;

},{}],12:[function(require,module,exports){
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

Object.defineProperty(exports, "__esModule", {
    value: true
});

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Viewmodel = function () {
    function Viewmodel() {
        _classCallCheck(this, Viewmodel);

        this.root = this.root || document;
        this.source = undefined;
        this.template = undefined;
        this.vdom = undefined;
        this.dependencies = this.constructor.dependencies.map(function (dep) {
            return new dep();
        });
    }

    _createClass(Viewmodel, [{
        key: 'querySelector',
        value: function querySelector() {
            var str = [];
            switch (Object.prototype.toString.call(this.constructor.selector)) {
                case '[object Function]':
                    this.constructor.selector(str);
                    break;
                case '[object Object]':
                    for (var key in this.constructor.selector) {
                        str.push('[' + key + (this.constructor.selector[key] ? '="' + this.constructor.selector[key] + '"]' : ']'));
                    }
                    break;
                default:
                    str.push(this.constructor.selector);
            }
            return str.join(',');
        }
    }, {
        key: 'query',
        value: function query() {
            var _this = this;

            this.source = Array.prototype.slice.call(this.root.querySelectorAll(this.querySelector()));
            this.source.forEach(function (node) {
                _this.dependencies.forEach(function (dep) {
                    dep.root = node;
                    dep.query();
                });
            });
            return this;
        }
    }, {
        key: 'parse',
        value: function parse() {
            var _this2 = this;

            this.template = this.source.map(function (source) {
                return windsock.parse(source);
            });
            this.template.forEach(function (template) {
                _this2.dependencies.forEach(function (dep) {
                    dep.parse(template.filter(dep.selector));
                });
            });
            return this;
        }
    }, {
        key: 'compile',
        value: function compile() {
            var _this3 = this;

            this.vdom = this.template.map(function (template) {
                return windsock.compile(template);
            });
            this.vdom.forEach(function (vdom) {
                _this3.dependencies.forEach(function (dep) {
                    dep.compile(vdom.filter(dep.selector));
                });
            });
            return this;
        }
    }, {
        key: 'transclude',
        value: function transclude() {
            this.vdom.forEach(function (vdom) {
                return windsock.transclude(vdom);
            });
            return this;
        }
    }, {
        key: 'render',
        value: function render() {
            this.query().parse().compile().transclude();
            return this.vdom;
        }
    }, {
        key: 'destroy',
        value: function destroy() {
            this.vdom.destroy();
        }
    }]);

    return Viewmodel;
}();

exports.default = Viewmodel;

Viewmodel.selector = '';
Viewmodel.dependencies = [];

},{}],13:[function(require,module,exports){
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _viewmodel = require('../viewmodel');

var _viewmodel2 = _interopRequireDefault(_viewmodel);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Field = function (_Viewmodel) {
    _inherits(Field, _Viewmodel);

    function Field() {
        _classCallCheck(this, Field);

        var _this = _possibleConstructorReturn(this, Object.getPrototypeOf(Field).call(this));

        _this.selector = 'field';
        return _this;
    }
    //this could be a potential problem:
    //if the field is rendered as a root vm
    //the vdom will be an array of fragments
    //if the vdom is rendered as a dependent
    //the vdom will be an array of elements

    _createClass(Field, [{
        key: 'parse',
        value: function parse(template) {
            if (!template) {
                _get(Object.getPrototypeOf(Field.prototype), 'parse', this).call(this);
            } else {
                this.template = template;
            }
            this.template.forEach(function (temp) {
                if (!temp.attributes['has-value']) {
                    temp.attributes['has-value'] = 'false';
                }
            });
            return this;
        }
    }, {
        key: 'compile',
        value: function compile(vdom) {
            if (!vdom) {
                _get(Object.getPrototypeOf(Field.prototype), 'compile', this).call(this);
            } else {
                this.vdom = vdom;
            }
            this.vdom.forEach(function (v) {
                var input = v.find('input');
                input.on('blur', function (evt) {
                    v.attributes['has-value'] = !!input.node.value.length;
                });
            });
            return this;
        }
    }]);

    return Field;
}(_viewmodel2.default);

exports.default = Field;

Field.selector = 'field';
Field.dependencies = [];

},{"../viewmodel":12}],14:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _viewmodel = require('../viewmodel');

var _viewmodel2 = _interopRequireDefault(_viewmodel);

var _auth = require('../auth');

var _auth2 = _interopRequireDefault(_auth);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Nav = function (_Viewmodel) {
    _inherits(Nav, _Viewmodel);

    function Nav() {
        _classCallCheck(this, Nav);

        return _possibleConstructorReturn(this, Object.getPrototypeOf(Nav).call(this));
    }

    return Nav;
}(_viewmodel2.default);

exports.default = Nav;

Nav.selector = 'nav';
Nav.dependencies = [];

},{"../auth":3,"../viewmodel":12}],15:[function(require,module,exports){
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _viewmodel = require('../viewmodel');

var _viewmodel2 = _interopRequireDefault(_viewmodel);

var _ajax = require('../ajax');

var _ajax2 = _interopRequireDefault(_ajax);

var _auth = require('../auth');

var _auth2 = _interopRequireDefault(_auth);

var _state = require('../state');

var _state2 = _interopRequireDefault(_state);

var _field = require('./field');

var _field2 = _interopRequireDefault(_field);

var _toast = require('../toast');

var _toast2 = _interopRequireDefault(_toast);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Signin = function (_Viewmodel) {
    _inherits(Signin, _Viewmodel);

    function Signin() {
        _classCallCheck(this, Signin);

        var _this = _possibleConstructorReturn(this, Object.getPrototypeOf(Signin).call(this));

        _this.ajax = new _ajax2.default({
            uri: '/api/auth'
        });
        return _this;
    }

    _createClass(Signin, [{
        key: 'compile',
        value: function compile() {
            var _this2 = this;

            _get(Object.getPrototypeOf(Signin.prototype), 'compile', this).call(this);
            this.vdom.forEach(function (vdom) {
                var form = vdom.find('form');
                form.on('submit', function (evt) {
                    var body = {};
                    evt.preventDefault();
                    form.filter('input').forEach(function (input) {
                        body[input.attributes.name] = input.node.value;
                    });
                    _this2.ajax.POST(body).then(function (response) {
                        //write auth to localstorage
                        //redirect to root
                        _auth2.default.token = response.token;
                        _state2.default.current = '';
                    }, function (e) {
                        _toast2.default.simple(e.message);
                    });
                });
            });
            return this;
        }
    }]);

    return Signin;
}(_viewmodel2.default);

exports.default = Signin;

Signin.selector = {
    signin: null
};
Signin.dependencies = [_field2.default];

},{"../ajax":1,"../auth":3,"../state":6,"../toast":11,"../viewmodel":12,"./field":13}],16:[function(require,module,exports){
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _viewmodel = require('../viewmodel');

var _viewmodel2 = _interopRequireDefault(_viewmodel);

var _ajax = require('../ajax');

var _ajax2 = _interopRequireDefault(_ajax);

var _auth = require('../auth');

var _auth2 = _interopRequireDefault(_auth);

var _state = require('../state');

var _state2 = _interopRequireDefault(_state);

var _field = require('./field');

var _field2 = _interopRequireDefault(_field);

var _toast = require('../toast');

var _toast2 = _interopRequireDefault(_toast);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Signin = function (_Viewmodel) {
    _inherits(Signin, _Viewmodel);

    function Signin() {
        _classCallCheck(this, Signin);

        var _this = _possibleConstructorReturn(this, Object.getPrototypeOf(Signin).call(this));

        _this.ajax = new _ajax2.default({
            uri: '/api/user'
        });
        return _this;
    }

    _createClass(Signin, [{
        key: 'compile',
        value: function compile() {
            var _this2 = this;

            _get(Object.getPrototypeOf(Signin.prototype), 'compile', this).call(this);
            this.vdom.forEach(function (vdom) {
                var form = vdom.find('form');
                form.on('submit', function (evt) {
                    var body = {};
                    evt.preventDefault();
                    form.filter('input').forEach(function (input) {
                        body[input.attributes.name] = input.node.value;
                    });
                    _this2.ajax.POST(body).then(function (response) {
                        //we get the user back?
                    }, function (e) {
                        _toast2.default.simple(e.message);
                    });
                });
            });
            return this;
        }
    }]);

    return Signin;
}(_viewmodel2.default);

exports.default = Signin;

Signin.selector = {
    signup: null
};
Signin.dependencies = [_field2.default];

},{"../ajax":1,"../auth":3,"../state":6,"../toast":11,"../viewmodel":12,"./field":13}],17:[function(require,module,exports){
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _viewmodel = require('../viewmodel');

var _viewmodel2 = _interopRequireDefault(_viewmodel);

var _toast = require('../toast');

var _toast2 = _interopRequireDefault(_toast);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Toast = function (_Viewmodel) {
    _inherits(Toast, _Viewmodel);

    function Toast() {
        _classCallCheck(this, Toast);

        return _possibleConstructorReturn(this, Object.getPrototypeOf(Toast).call(this));
    }

    _createClass(Toast, [{
        key: 'parse',
        value: function parse() {
            _get(Object.getPrototypeOf(Toast.prototype), 'parse', this).call(this);
            this.template[0].children[0].attributes.display = false;
            this.template[0].children[0].on('transitionend', function () {
                this.resolve && this.resolve();
                this.resolve = null;
            });
            return this;
        }
    }, {
        key: 'compile',
        value: function compile() {
            _get(Object.getPrototypeOf(Toast.prototype), 'compile', this).call(this);
            _toast2.default.vm = this;
            return this;
        }
    }, {
        key: 'display',
        value: function display(message) {
            var _this2 = this;

            return new Promise(function (resolve, reject) {
                var elm = _this2.vdom[0].children[0];
                window.setTimeout(function () {
                    elm.resolve = resolve;
                    elm.attributes.display = false;
                }, 3000);
                elm.text = message;
                elm.attributes.display = true;
            });
        }
    }]);

    return Toast;
}(_viewmodel2.default);

exports.default = Toast;

Toast.selector = {
    toast: null
};
Toast.dependencies = [];

},{"../toast":11,"../viewmodel":12}]},{},[4]);
