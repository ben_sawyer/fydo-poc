var url = require('url'),
    http = require('http'),
    path = require('path'),
    fs = require('fs'),
    GoogleLocations = require('google-locations'),
    Yelp = require('yelp'),
    windsock = require('windsock');

var locations = new GoogleLocations('AIzaSyDW-BOkJ3qalg0TwwNh40kMnK07kQFH_vc');

var yelp = new Yelp({
    consumer_key: 'iWv9PYIdqNdV2N6kBCK-Dw',
    consumer_secret: '-nMo-PchxS3XtUp5vQ0rJoYuTdM',
    token: 'ShpRaJjBs2u8KRrqjvz_5jcfhRIM2DvI',
    token_secret: '7Xg5GCvvbU0RGK-IBBto7GEtDKU',
});

var httpServer,
    listenPort = process.env.PORT || '8080',
    PUBLIC_DIR = path.resolve(__dirname, 'build'),
    proxyConfig = [{
        url: '/location',
        method:{
            'GET': function(req, res){
                console.log(url.parse(req.url, true).query);
                locations.autocomplete({
                    input: url.parse(req.url, true).query.input || '',
                    types: "(cities)"
                }, function(err, data){
                    var template = '<li predictionId="id" class=""><p>description</p></li>',
                        parent = repeat(template, data.predictions, function(v){return v.toString().replace(', United States','');});
                    res.writeHead(200,  {'Content-Type': 'application/json'});
                    res.write(JSON.stringify(parent));
                    res.end();
                });
            }
        }
    },{
        url: '/search',
        method:{
            'GET': function(req, res){
                console.log(url.parse(req.url, true).query);
                yelp.search(url.parse(req.url, true).query)
                    .then(function(data){
                        var template = '<li businessId="id" lat="location.coordinate.latitude" long="location.coordinate.longitude" class=""><h1>name</h1><p>display_phone</p><p>location.display_address</p></li>',
                            parent = repeat(template, data.businesses);
                        res.writeHead(200,  {'Content-Type': 'application/json'});
                        res.write(JSON.stringify(parent));
                        res.end();
                    })
                    .catch(function(err){
                        console.error(err);
                        res.writeHead(404);
                        res.end();
                    });
            }
        }
    }],
    mimeTypes = {
        html: 'text/html',
        jpeg: 'image/jpeg',
        jpg: 'image/jpeg',
        png: 'image/png',
        js: 'text/javascript',
        css: 'text/css',
        woff2: 'application/font-woff2',
        json: 'application/json'
    };

function resolve(path, data){
    var keys = path.split('.'),
        resolved = data;
    keys.forEach(function(key){
        resolved = resolved[key];
    });
    return resolved;
}

httpServer = http.createServer();
httpServer.on('request', httpRequest);

console.log('server listening on port ' + listenPort);
console.log('server public directory', PUBLIC_DIR);

function httpRequest(req, res){
    var pathName = url.parse(req.url).pathname,
        proxyRequest;

    console.log('http ' + req.method + ':', pathName);

    for(var i = 0; i < proxyConfig.length; i ++){
        if(req.url.indexOf(proxyConfig[i].url) === 0){
            if(proxyConfig[i].method[req.method]){
                proxyRequest = proxyConfig[i].method[req.method];
                console.log('proxying ' + req.method + ':', pathName);
                proxyRequest(req, res);
                return;
            }
            //no method configured - return not found
            res.writeHead(404);
            res.end();
            return;
        }
    }
    switch (pathName){
        case '/':
            index(req, res);
        break;
        default:
            readStaticFile(pathName, req, res);
        break;
    }
}

function index(req, res){
    res.writeHead(200,  {'Content-Type': 'text/html'});
    fs.createReadStream(path.join(PUBLIC_DIR , '/index.html')).pipe(res);
    return;
}

function readStaticFile(fileName, req, res){
    var contentType;
    fileName = path.join(PUBLIC_DIR, fileName);
    fs.exists(fileName, function(exists) {
        //check file extension/type here too
        if (exists) {
            contentType = mimeTypes[path.extname(fileName).split(".")[1]];
            res.writeHead(200, {
                'Content-Type': contentType
            });
            fs.createReadStream(fileName).pipe(res);
            console.log('resolved file:', fileName);
            return;
        } else {
            res.statusCode = 404;
            res.end('404 Not Found. Sorry.\n');
            console.log('unable to resolve file:', fileName);
            return;
        }
    });
}

function repeat(template, data, transform){
    transform = transform || function(v){return v;};
    var itemFragment = windsock.parse(template),
        parent = windsock.parse('<div></div>').children[0];
    data.forEach(function(item){
        var clone = itemFragment.children[0].clone(true);
        for(var key in clone.attributes){
            if(clone.attributes[key]){
                clone.attributes[key] = resolve(clone.attributes[key], item);
            }
        }
        clone.children.forEach(function(child){
            var resolved = resolve(child.text, item);
            if(Object.prototype.toString.call(resolved) === '[object Array]'){
                child.text = '';
                resolved.forEach(function(res, i){
                    var frag = windsock.parse(transform(res) + '<br>');
                    child.append(frag.children[0]);
                    if(i < resolved.length - 1) child.append(frag.children[0]);
                });
            }else if(Object.prototype.toString.call(resolved) === '[object Objet]'){
                //derp
            }else{
                child.text = resolved ? transform(resolved.toString()) : '';
            }
        });
        parent.append(clone);
    });
    return parent;
}

httpServer.listen(listenPort);
